magento-cache-cleaner.sh

A simple command line tool designed to be run on a cron job every hour to clean up the Magento file cache.

You probably shouldn't be using the Magento file cache.. It's not very good and it NEVER seems to delete ANYTHING!

