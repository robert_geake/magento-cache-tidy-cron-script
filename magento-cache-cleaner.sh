#!/bin/sh

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# USE AT YOUR OWN RISK

# remove cache files older than 2 hours
find /var/www/html/prod/var/cache/ -type f -mmin +120 -exec rm {} \;
# remove session files older than 2 hours
find /var/www/html/prod/var/session/ -type f -mmin +120 -exec rm {} \;
# remove report files older than 7 days
find /var/www/html/prod/var/report/ -mtime +7 -exec rm -f {} \;
# remove temp file older than 1 day
find /var/www/html/prod/var/tmp/ -mtime +1 -exec rm -f {} \;

